---
title: Commands
tags: [server]
summary: "An overview of the commands offered by the psono server"
sidebar: mydoc_sidebar
permalink: mydoc_other_commands.html
folder: mydoc
---

## Commands

The psono server offers some commands:


### cleartoken

Clears all expired tokens

	python3 ./psono/manage.py cleartoken

(usually executed in  a cronjob)


### createuser

Creates a user with a given username, password and email

	python3 ./psono/manage.py createuser username@example.com myPassword email@something.com

(usually executed by developers when they want to create a new user for testing)


### deleteuser

Deletes a user with a given username

	python3 ./psono/manage.py deleteuser username@example.com

(usually executed by developers when they want to cleanup after testing, or want to try again with a fresh user)


### disableuser

Disables a user. The user won't be able to login anymore.

	python3 ./psono/manage.py disableuser username@example.com

### enableuser

Enables a user. The user will be able to login again.

	python3 ./psono/manage.py enableuser username@example.com

### promoteuser

Promotes a user and grants superuser privilege (or other roles)

	python3 ./psono/manage.py promoteuser username@example.com superuser

### demoteuser

Demotes a user and revokes superuser privilege (or other roles)

	python3 ./psono/manage.py demoteuser username@example.com superuser

### generateserverkeys

Generates some new server keys for the settings.yaml

	python3 ./psono/manage.py generateserverkeys

(usually executed only once during the installation phase)

### presetup

If the database user has the necessary permission this command will install the necessary postgres extensions ltree and pgcrypto.

	python3 ./psono/manage.py presetup

(usually executed only once during the installation phase)


### sendtestmail

Sends a test email to the given email address.

	python3 ./psono/manage.py sendtestmail something@something.com

(usually executed only during isntallation, to test if the email configuration is correct)


### testldap

Tests your LDAP connection (EE Version only)

	python3 ./psono/manage.py testldap username@something.com thePassWord

(usually executed only during isntallation, to test if the LDAP configuration is correct)


