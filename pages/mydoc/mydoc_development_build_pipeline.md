---
title: Build Pipeline
tags: [development, contribution]
summary: "An overview of the build pipeline for the server and the client."
sidebar: mydoc_sidebar
permalink: mydoc_development_build_pipeline.html
folder: mydoc
---

The client and the server have both an automated build pipeline for gitlab runner, automating the process for:

- unit testing and coverage calculation
- security and vulnerability scanner
- building and packaging
- uploading and releasing

## Sequence diagram client:

![Sequence diagram of the build pipeline of the Psono Client](images/Build_Pipeline_Client.png)

## Sequence diagram server:

![Sequence diagram of the build pipeline of the Psono Server](images/Build_Pipeline_Server.png)

{% include links.html %}

